#!/usr/bin/env python

"""
* Assignment 2 - AI
* Group 12
* This is a GA approach solution to the traveling salesman problem.
"""

import math
import random


class CityNode:
    def __init__(self, x=None, y=None):
        self.x = None
        self.y = None
        if x is not None:
            self.x = x
        else:
            self.x = int(random.random() * 200)
        if y is not None:
            self.y = y
        else:
            self.y = int(random.random() * 200)

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def dist_to(self, city):
        x_distance = abs(self.get_x() - city.get_x())
        y_distance = abs(self.get_y() - city.get_y())
        distance = math.sqrt((x_distance * x_distance) + (y_distance * y_distance))
        return distance

    def __repr__(self):
        return str(self.get_x()) + ", " + str(self.get_y())


class NodeManager:
    destinationCities = []

    def add_city(self, city):
        self.destinationCities.append(city)

    def get_city(self, index):
        return self.destinationCities[index]

    def number_of_cities(self):
        return len(self.destinationCities)


class NodeVisit:
    def __init__(self, node_manager, tour=None):
        self.node_manager = node_manager
        self.tour = []
        self.fitness = 0.0
        self.distance = 0
        if tour is not None:
            self.tour = tour
        else:
            for i in range(0, self.node_manager.number_of_cities()):
                self.tour.append(None)

    def __len__(self):
        return len(self.tour)

    def __getitem__(self, index):
        return self.tour[index]

    def __setitem__(self, key, value):
        self.tour[key] = value

    def __repr__(self):
        gene_string = "|"
        for i in range(0, self.num_of_nodes()):
            gene_string += str(self.get_city(i)) + "|"
        return gene_string

    def gen_single(self):
        for cityIndex in range(0, self.node_manager.number_of_cities()):
            self.set_city(cityIndex, self.node_manager.get_city(cityIndex))
        random.shuffle(self.tour)

    def get_city(self, node_visit_pos):
        return self.tour[node_visit_pos]

    def set_city(self, node_visit_pos, city):
        self.tour[node_visit_pos] = city
        self.fitness = 0.0
        self.distance = 0

    def get_fit(self):
        if self.fitness == 0:
            self.fitness = 1 / float(self.get_dist())
        return self.fitness

    def get_dist(self):
        if self.distance == 0:
            node_visit_dist = 0
            for cityIndex in range(0, self.num_of_nodes()):
                from_city = self.get_city(cityIndex)
                dest_city = None
                if cityIndex + 1 < self.num_of_nodes():
                    dest_city = self.get_city(cityIndex + 1)
                else:
                    dest_city = self.get_city(0)
                node_visit_dist += from_city.dist_to(dest_city)
            self.distance = node_visit_dist
        return self.distance

    def num_of_nodes(self):
        return len(self.tour)

    def contains_city(self, city):
        return city in self.tour


class Population:
    def __init__(self, node_manager, pop_size, initialise):
        self.tours = []
        for i in range(0, pop_size):
            self.tours.append(None)

        if initialise:
            for i in range(0, pop_size):
                new_node_visit = NodeVisit(node_manager)
                new_node_visit.gen_single()
                self.save_node_visit(i, new_node_visit)

    def __setitem__(self, key, value):
        self.tours[key] = value

    def __getitem__(self, index):
        return self.tours[index]

    def save_node_visit(self, index, tour):
        self.tours[index] = tour

    def get_node_visit(self, index):
        return self.tours[index]

    def get_fittest(self):
        fittest = self.tours[0]
        for i in range(0, self.population_size()):
            if fittest.get_fit() <= self.get_node_visit(i).get_fit():
                fittest = self.get_node_visit(i)
        return fittest

    def population_size(self):
        return len(self.tours)


# Main function for mutation, evolution, crossover, and fittest selection.
class GA:
    def __init__(self, node_manager):
        self.node_manager = node_manager

        # Increasing this leads to very mutated children, making the accuracy lower
        self.mutationRate = 0.015

        # The number of choices/battles for the best parent1 and parent2
        self.tournamentSize = 5

        # enable 'elitism'
        self.elitism = True

    def evolve_population(self, pop):
        new_population = Population(self.node_manager, pop.population_size(), False)

        # How we select the best two points of the population
        elitism_offset = 0
        if self.elitism:
            new_population.save_node_visit(0, pop.get_fittest())
            elitism_offset = 1

        for i in range(elitism_offset, new_population.population_size()):
            parent1 = self.node_visit_selection(pop)
            parent2 = self.node_visit_selection(pop)
            child = self.crossover(parent1, parent2)
            new_population.save_node_visit(i, child)

        for i in range(elitism_offset, new_population.population_size()):
            self.mutate(new_population.get_node_visit(i))

        return new_population

    def crossover(self, parent1, parent2):
        child = NodeVisit(self.node_manager)

        start_pos = int(random.random() * parent1.num_of_nodes())
        end_pos = int(random.random() * parent1.num_of_nodes())

        for i in range(0, child.num_of_nodes()):
            if start_pos < end_pos and start_pos < i < end_pos:
                child.set_city(i, parent1.get_city(i))
            elif start_pos > end_pos:
                if not (start_pos > i > end_pos):
                    child.set_city(i, parent1.get_city(i))

        for i in range(0, parent2.num_of_nodes()):
            if not child.contains_city(parent2.get_city(i)):
                for ii in range(0, child.num_of_nodes()):
                    if child.get_city(ii) is None:
                        child.set_city(ii, parent2.get_city(i))
                        break

        return child

    def mutate(self, tour):
        for tour_pos_normal in range(0, tour.num_of_nodes()):
            if random.random() < self.mutationRate:
                tour_pos_mutated = int(tour.num_of_nodes() * random.random())

                city1 = tour.get_city(tour_pos_normal)
                city2 = tour.get_city(tour_pos_mutated)

                tour.set_city(tour_pos_mutated, city1)
                tour.set_city(tour_pos_normal, city2)

    def node_visit_selection(self, pop):
        tournament = Population(self.node_manager, self.tournamentSize, False)
        for i in range(0, self.tournamentSize):
            rand_id = int(random.random() * pop.population_size())
            tournament.save_node_visit(i, pop.get_node_visit(rand_id))
        fittest = tournament.get_fittest()
        return fittest


# get the diff between two numbers and return percentage
def get_change(current, previous):
    if current == previous:
        return 100.0
    try:
        return (abs(current - previous) / previous) * 100.0
    except ZeroDivisionError:
        return 0


if __name__ == '__main__':
    
    # create manager
    nodemanager = NodeManager()

    # create cities
    cities = [(20, 20), (20, 40), (60, 80), (120, 40), (20, 160), (60, 200),
     (80, 180), (100, 160), (100, 120), (140, 180), (180, 200), 
     (200, 160), (180, 100), (140, 140), (120, 80), (180, 60), (200, 40),
     (160, 20), (100, 40), (60, 20), (20, 20)]

    # push onto node manager
    for city in cities:
        nodemanager.add_city(CityNode(city[0], city[1]))
        
    # Initialize population
    pop_current = Population(nodemanager, 1000, True);
    pop_previous = 0
    print("Initial distance: " + str(pop_current.get_fittest().get_dist()))

    # Evolve population for 50 generations
    ga = GA(nodemanager)
    pop_current = ga.evolve_population(pop_current)
    for i in range(0, 1000):
        pop_previous = pop_current
        pop_current = ga.evolve_population(pop_current)

        # Termination: When the diff between them is == 0, break the loop
        if get_change(pop_current.get_fittest().get_dist(), pop_previous.get_fittest().get_dist() == 0):
            break

    # Print final results
    print("Finished")
    print("Final distance: " + str(pop_current.get_fittest().get_dist()))
    print("Solution:")
    print(pop_current.get_fittest())
    
    # visualization
    initial_x = [x[0] for x in cities]
    initial_y = [y[1] for y in cities]
    result = list(pop_current.get_fittest())
    new_x, new_y = [], []
    for i in result:
        new_x.append(int(str(i).split(',')[0]))
        new_y.append(int(str(i).split(',')[1].strip()))
    
    new_x.append(new_x[0])
    new_y.append(new_y[0])
    
    # Initial Graph
    plt.subplot(1, 2, 1);
    plt.scatter(initial_x, initial_y);
    plt.title('Initial')

    # Genetic Graph
    plt.subplot(1, 2, 2);
    plt.plot(new_x, new_y, 'g', linewidth = 0.8);
    plt.scatter(new_x, new_y);
    plt.title('Genetic')

    plt.tight_layout()



